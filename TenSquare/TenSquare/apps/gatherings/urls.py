from django.urls import re_path
from . import views

urlpatterns = [
    re_path(r'^gatherings/$', views.GatheringAPIView.as_view()),
    re_path(r'^gatherings/(?P<pk>\d+)/$', views.GatheringDetailAPIView.as_view()),
    re_path(r'^gatherings/(?P<pk>\d+)/join/$', views.GatheringJoinAPIView.as_view()),
]