from rest_framework import serializers
from gatherings.models import Gathering


class GatheringSerializer(serializers.ModelSerializer):

    users = serializers.PrimaryKeyRelatedField(read_only=True, many=True)

    class Meta:
        model = Gathering
        fields = '__all__'

