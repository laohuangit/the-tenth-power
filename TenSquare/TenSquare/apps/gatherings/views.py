from rest_framework.response import Response
from rest_framework.views import APIView

from gatherings.models import Gathering
from gatherings.serializer import GatheringSerializer


class GatheringAPIView(APIView):
    def get(self, request):
        gatherings = Gathering.objects.all()
        count = Gathering.objects.all().count()
        serializer = GatheringSerializer(gatherings, many=True)
        print(serializer.data)
        return Response({
            "count": count,
            "results": serializer.data,
            "next": '',
            "previous": '',
        })

# 活动详情展示
class GatheringDetailAPIView(APIView):

    def get(self, request, pk):

        gathering = Gathering.objects.get(pk=pk)

        serializer = GatheringSerializer(gathering)

        return Response(serializer.data)

# 活动报名


class GatheringJoinAPIView(APIView):

    def post(self, request, pk):

        gathering = Gathering.objects.get(pk=pk)

        obj = Gathering.objects.filter(users=request.user.id)
        if obj:
            gathering.users.remove(request.user.id)
            return Response({
                'message': '取消报名',
                'success': True
            })

        else:
            gathering.users.add(request.user.id)
            return Response({
                'message': '报名成功',
                'success': True
            })


