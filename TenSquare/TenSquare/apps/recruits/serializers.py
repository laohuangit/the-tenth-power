from rest_framework import serializers
from rest_framework.relations import PrimaryKeyRelatedField
from recruits.models import Recruit, Enterprise, City


class EnterpriseSerializer(serializers.ModelSerializer):
    recruits = PrimaryKeyRelatedField(label='职位数', queryset=Recruit.objects.all(), many=True)

    class Meta:
        model = Enterprise
        exclude = ('content', 'city', 'address', 'coordinate', 'url', 'visits', 'users')


class RecruitSerializer(serializers.ModelSerializer):
    enterprise = EnterpriseSerializer(label='公司')

    class Meta:
        model = Recruit
        exclude = ('address', 'state', 'detailcontent', 'detailrequire', 'visits', 'users')


class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = '__all__'


class EntSerializer(serializers.ModelSerializer):
    recruits = RecruitSerializer(many=True)
    users = serializers.StringRelatedField()

    class Meta:
        model = Enterprise
        exclude = ()


class ReSerializer(serializers.ModelSerializer):
    users = serializers.StringRelatedField(many=True)
    enterprise = EntSerializer()

    class Meta:
        model = Recruit
        fields = '__all__'
