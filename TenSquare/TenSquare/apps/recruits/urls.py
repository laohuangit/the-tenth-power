from django.urls import re_path

from recruits import views

urlpatterns = [
    # 进行 URL 配置
    re_path(r'^recruits/search/recommend/$', views.PermissionViewSet.as_view()),
    re_path(r'^recruits/search/latest/$', views.PermissionViewSet.as_view()),
    re_path(r'^enterprise/search/hotlist/$', views.EnterpriseViewSet.as_view()),
    re_path(r'^city/hotlist/$', views.CityViewSet.as_view()),
    re_path(r'^recruits/(?P<pk>\d+)/$', views.RecruitsViewSet.as_view()),
    re_path(r'^recruits/search/city/keyword/$', views.KeywordViewSet.as_view()),
    re_path(r'^recruits/(?P<pk>\d+)/visit/$', views.AVisitViewSet.as_view()),
    # re_path(r'^recruits/(?P<pk>\d+)/collect/$', views.CollectViewSet.as_view()),
    re_path(r'^enterprise/(?P<pk>\d+)/$', views.EnterpriseView.as_view()),
    re_path(r'^enterprise/(?P<pk>\d+)/visit/$', views.EVisitViewSet.as_view()),
]
