from django.urls import re_path
from rest_framework.routers import SimpleRouter
from . import views


urlpatterns = [
]

router = SimpleRouter()

router.register(r'spit', views.SpitView)

urlpatterns += router.urls
