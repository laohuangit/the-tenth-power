from django.conf.urls import url
from rest_framework_jwt.views import obtain_jwt_token
from django.urls import re_path
from . import views

urlpatterns = [
    url(r'^authorizations/$', obtain_jwt_token),  # 登录

    re_path(r'^sms_codes/(?P<mobile>1[3-9]\d{9})/$', views.SMSCodeView.as_view()),  # 短信验证码
    # re_path(r'^users/$', views.UserRegister.as_view()),  # 注册
    re_path(r'^users/$', views.UserListView.as_view()),  # 注册

    re_path(r'^user/$', views.UserMessage.as_view()),  # 获取个人详情
    # re_path(r'^user/password/$', views.ChangePassword.as_view()),  # 修改密码

    re_path(r'^users/like/(?P<id>\d+)/$', views.FollowUsers.as_view()),
]