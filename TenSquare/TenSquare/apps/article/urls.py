from django.conf.urls import re_path
from django.urls import re_path
from article.views.channel import ChannelViewSet
from article.views.article import ArticleViewSet, LabelShowViewSet, ArticlePostViewSet, ArticlaSearchViewSet
from article.views.collect import CollectedViewSet
# from article.views.comment import CommentViewSet
from article.views.articleInfo import ArticleDetailView
from article.views.comment import CommentViewSet

urlpatterns = [
    re_path(r'^channels/$', ChannelViewSet.as_view()),
    re_path(r'^article/(?P<id>-?\d+)/channel/', ArticleViewSet.as_view()),
    re_path(r'^article/(?P<id>\d+)/collect/$', CollectedViewSet.as_view()),
    re_path(r'^labels/$', LabelShowViewSet.as_view()),
    re_path(r'^article/$', ArticlePostViewSet.as_view()),
    re_path(r'^article/(?P<id>\d+)/publish_comment/$', CommentViewSet.as_view()),
    re_path(r'^article/(?P<pk>-?\d+)/$', ArticleDetailView.as_view()),
    re_path(r'^articles/search/$', ArticlaSearchViewSet.as_view())
]