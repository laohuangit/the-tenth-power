from rest_framework import serializers
from rest_framework.relations import PrimaryKeyRelatedField

from article.models import Article, Comment
from questions.models import Label
from users.models import User


class ArticleoneSerializer(serializers.ModelSerializer):
    class Meta:
        model = Article
        fields = ('id', 'title')


class UserSerializer(serializers.ModelSerializer):
    articles = ArticleoneSerializer(label='用户')

    class Meta:
        model = User
        fields = ('id', 'username','avatar', 'articles', 'fans')


class ArticleSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    
    class Meta:
        model = Article
        fields = ('id', 'title', 'content', 'createtime', 'user', 'collected_users', 'image', 'visits')


class LabelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Label
        fields = ('id', 'label_name')


class ArticalSearchSerializer(serializers.ModelSerializer):
    text = serializers.StringRelatedField(label='')

    class Meta:
        model = Article
        fields = ('id', 'title', 'content', 'text', 'createtime')



