from rest_framework import serializers

from article.models import Article, Comment
from users.models import User


class UserSimpleArticleSerializer(serializers.ModelSerializer):
    """文章作者对应的文章信息"""

    class Meta:
        model = Article
        fields = ('id', 'title')


class ArticleUserSerializer(serializers.ModelSerializer):
    """文章对应的作者信息"""
    articles = UserSimpleArticleSerializer(label='文章作者对应的文章信息', many=True)
    fans = serializers.PrimaryKeyRelatedField(read_only=True, many=True)

    class Meta:
        model = User
        fields = ('id', 'username', 'avatar', 'articles', 'fans')


# class SubsArticleCommentsSerializer(serializers.ModelSerializer):
#     """子评论的信息"""
#     user = ArticleUserSerializer(label='文章对应的作者信息')
#     subs = serializers.StringRelatedField(label='', many=True)
#
#     class Meta:
#         model = Comment
#         fields = '__all__'
#
#
# class ArticleCommentsSerializer(serializers.ModelSerializer):
#     """评论信息"""
#     user = ArticleUserSerializer(label='文章对应的作者信息')
#     subs = SubsArticleCommentsSerializer(label='父评论们', many=True)
#
#     class Meta:
#         model = Comment
#         fields = '__all__'
#
#
# class ArticleDetailSerializer(serializers.ModelSerializer):
#     """文章详情信息"""
#     user = ArticleUserSerializer(label='文章对应的作者信息')
#     comments = ArticleCommentsSerializer(label='文章的评论', many=True)
#     labels = serializers.PrimaryKeyRelatedField(label='标签', many=True, read_only=True)
#     collected_users = serializers.PrimaryKeyRelatedField(label='收藏用户姓名', many=True, read_only=True)
#
#     class Meta:
#         model = Article
#         fields = '__all__'

class ComSerializer(serializers.ModelSerializer):
    user = UserSimpleArticleSerializer()
    subs = serializers.StringRelatedField(many=True)

    class Meta:
        model = Comment
        fields = ('id', 'subs', 'content', 'user', 'createtime', 'parent', 'article')


class CommentSerializer(serializers.ModelSerializer):
    user = UserSimpleArticleSerializer()
    subs = ComSerializer(many=True)

    class Meta:
        model = Comment
        fields = ('id', 'subs', 'content', 'user', 'createtime', 'updatetime', 'parent', 'article')


class ArtSerializer(serializers.ModelSerializer):
    user = UserSimpleArticleSerializer()
    comments = CommentSerializer(many=True)

    class Meta:
        model = Article
        exclude = ()