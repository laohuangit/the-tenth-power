from rest_framework.response import Response
from rest_framework.views import APIView
from article.models import Article


class CollectedViewSet(APIView):

    def put(self, request, id):
        article = Article.objects.get(id=id)
        user = request.user
        if user in article.collected_users.all():
            article.collected_users.remove(user)
            response_data = {
                "success": False,
                "message": "取消收藏"
            }
        else:
            article.collected_users.add(user)
            response_data = {
                "success": True,
                "message": "收藏成功"
            }
        return Response(response_data)
