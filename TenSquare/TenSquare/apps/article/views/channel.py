from rest_framework.generics import ListAPIView
from article.models import Channel
from article.serializers.channel import ChannelSerializer


#  /channels/
class ChannelViewSet(ListAPIView):
    serializer_class = ChannelSerializer

    def get_queryset(self):
        channels = Channel.objects.all()
        return channels

