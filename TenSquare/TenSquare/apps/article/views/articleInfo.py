from rest_framework.generics import RetrieveAPIView

from article.models import Article
from article.serializers.articleinfo import ArtSerializer


class ArticleDetailView(RetrieveAPIView):
    """根据id获取文章详细信息"""
    queryset = Article.objects.all()
    serializer_class = ArtSerializer