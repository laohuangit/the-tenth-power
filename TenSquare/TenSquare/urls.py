"""TenSquare URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from django.conf.urls import url

urlpatterns = [
    path('admin/', admin.site.urls),
    path(r'', include('users.urls')),
    path(r'', include('article.urls')),
    path(r'', include('gatherings.urls')),
    # path(r'', include('questions.urls')),
    path(r'', include('recruits.urls')),
    path(r'', include('spit.urls')),
    path(r'', include('upload.urls')),
    url(r'ckeditor/', include('ckeditor_uploader.urls')), #添加ckeditor的url到项目中
    # static(dev.MEDIA_URL, document_root=settings.MEDIA_ROOT)
]
